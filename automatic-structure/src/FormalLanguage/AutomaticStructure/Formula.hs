{-# LANGUAGE Rank2Types #-}

module FormalLanguage.AutomaticStructure.Formula (
  Var (..),
  Formula (..),

  exists, forall, not, (\/), (/\),

  freeVars, universalClosure
) where

import Prelude hiding (not, foldl)
import qualified Data.Set as S
import Data.Vector (Vector, foldl)
import FormalLanguage.Regular

type Var = Int

data Formula sym
  = Exists Var (Formula sym)
  | Not (Formula sym)
  | Or (Formula sym) (Formula sym)
  | Rel (Regular (Vector sym)) (Vector Var)

instance (Show sym) => Show (Formula sym) where
  --show (Not (Exists i (Not phi))) = "∀x" ++ show i ++ " " ++ show phi
  show (Exists i phi) = "∃x" ++ show i ++ " " ++ show phi
  show (Not (Or (Not phi) (Not psi))) = "(" ++ show phi ++ " ∧ " ++ show psi ++ ")"
  show (Not phi) = "¬" ++ show phi
  show (Or phi psi) = "(" ++ show phi ++ " ∨ " ++ show psi ++ ")"
  show (Rel _ vars) = "R(" ++ tail (tail $ show' vars) ++ ")" --TODO named relations
    where show' = foldl (\s v -> s ++ ", x" ++ show v) ""

exists = Exists
forall i = not . (exists i) . not
not = Not
(\/) = Or
phi /\ psi = not (not phi \/ not psi)

freeVars (Exists i phi) = freeVars phi S.\\ S.singleton i
freeVars (Not phi) = freeVars phi
freeVars (Or phi psi) = S.union (freeVars phi) (freeVars psi) 
freeVars (Rel _ vars) = foldl (\xs x -> S.insert x xs) S.empty vars

universalClosure phi = S.fold forall phi $ freeVars phi
