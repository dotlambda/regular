module FormalLanguage.AutomaticStructure (
  decide, alphabet, wellformed, fulfilling
) where

import Prelude hiding (union)
import Data.Maybe
import qualified Data.Set as S
import qualified Data.Map as M
import qualified Data.Vector as V
import qualified FormalLanguage.Regular as R
import FormalLanguage.AutomaticStructure.Formula as Formula hiding (not)

union xs ys = xs V.++ (V.filter (`V.notElem` xs) ys)

-- http://buffered.io/posts/a-better-nub/
nub :: (Ord a) => (V.Vector a) -> (V.Vector a)
nub = V.fromList . go S.empty
  where go s v | V.null v        = []
               | el `S.member` s = go s $ V.unsafeTail v
               | otherwise       = el : go (S.insert el s) (V.unsafeTail v)
               where el = V.unsafeHead v

alphabet sigma n = S.fromList $ map V.fromList $ alphabet' n
  where alphabet' 1 = map (\x -> [x]) sigma'
        alphabet' n = [ x : xs | x <- sigma', xs <- alphabet' (n-1) ]
        sigma' = S.toList sigma

-- Hodgeson calls this language ({#}^n)*
onlySharp sharp n = R.Dfa (M.singleton ((), V.replicate n sharp) ()) () (S.singleton ())

-- the langauge containing every well formed word over the alphabet, D_n^#
wellformed :: (Ord sym) => S.Set sym -> sym -> R.Regular sym -> Int -> R.Regular (V.Vector sym)
wellformed sigma sharp domain n = R.unions [reg i | i <- [0..n-1]]
  where onlySharp = R.Dfa (M.singleton ((), sharp) ()) () (S.singleton ()) -- {#}*
        reg i = R.intersects
          ((R.applyInvStrictHom alphabet' (proj i) domain) :
          [R.applyInvStrictHom alphabet' (proj j) domainSharp | j <- [0..n-1], j /= i])
        proj i xs = xs V.! i
        domainSharp = R.toDfa $ domain `R.concat` onlySharp
        alphabet' = alphabet sigma n

fulfilling :: (Ord sym) => S.Set sym -> sym -> (Int -> R.Regular (V.Vector sym)) -> Formula sym -> (V.Vector Var, R.Regular (V.Vector sym))
fulfilling sigma sharp wellformed (Not (Not phi)) = fulfilling sigma sharp wellformed phi
fulfilling sigma sharp wellformed (Not phi) = (vars, wellformed (V.length vars) R.\\ reg)
  where (vars, reg) = fulfilling sigma sharp wellformed phi
fulfilling sigma sharp wellformed (phi `Or` psi) = (vars, reg)
  where (vars1, reg1) = fulfilling sigma sharp wellformed phi
        (vars2, reg2) = fulfilling sigma sharp wellformed psi
        vars = union vars1 vars2
        proj1 = V.take (V.length vars1)
        proj2 xs = V.map (\x -> xs V.! (fromJust $ V.elemIndex x vars)) vars2
        reg = (R.applyInvStrictHom alphabet' proj1 (reg1 `R.concat` onlySharp sharp (V.length vars1))
                `R.union` (R.applyInvStrictHom alphabet' proj2 (reg2 `R.concat` onlySharp sharp (V.length vars2))))
                `R.intersect` (wellformed $ V.length vars)
        alphabet' = alphabet sigma (V.length vars)
fulfilling sigma sharp wellformed (Exists x phi) = (vars', reg')
  where (vars, reg) = fulfilling sigma sharp wellformed phi
        index = fromJust $ V.elemIndex x vars
        len = V.length vars'
        vars' = V.filter (/= x) vars
        proj xs = left V.++ (V.tail right)
          where (left, right) = V.splitAt index xs
        reg' = (R.quotient (R.applyStrictHom proj reg) (onlySharp sharp len)) `R.intersect` (wellformed len)
fulfilling sigma _ _ (Rel reg vars) = if V.length vars' == V.length vars then (vars, reg) else (vars', reg')
  where vars' = nub vars
        proj xs = V.map (\x -> xs V.! (fromJust $ V.elemIndex x vars')) vars
        reg' = R.applyInvStrictHom (alphabet sigma $ V.length vars') proj reg

-- decide a sentence
--decide' :: [sym] -> R.Regular sym -> Formula sym -> Bool
decide' sigma sharp wellformed (Not phi) = not $ decide' sigma sharp wellformed phi
decide' sigma sharp wellformed (Or phi psi) = decide' sigma sharp wellformed phi || decide' sigma sharp wellformed psi
decide' sigma sharp wellformed (Exists _ phi) = not $ R.empty $ snd $ fulfilling sigma sharp wellformed phi

--decide :: [sym] -> R.Regular sym -> Formula sym -> Bool
-- sharp is the sign to convolute with
decide sigma sharp wellformed phi = decide' sigma sharp wellformed $ universalClosure phi
