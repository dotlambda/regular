import Prelude hiding (not)
import qualified Data.Vector as V
import qualified Data.Set.Extra as S
import qualified Data.Map as M
import FormalLanguage.Regular
import FormalLanguage.AutomaticStructure hiding (wellformed)
import FormalLanguage.AutomaticStructure.Formula

sigma = S.fromList [0,1]

wellformed :: (Ord sym) => S.Set sym -> sym -> Int -> Regular (V.Vector sym)
wellformed sigma sharp n = Dfa delta 0 (S.fromList [1,2])
  where alphabet' 1 = [[sym] | sym <- S.toList sigma]
        alphabet' n = [sym : syms | sym <- S.toList sigma, syms <- alphabet' (n-1)]
        alphabet = map V.fromList $ alphabet' n
        delta = M.fromList $ [((0, v), 1) | v <- alphabet]
                  ++ [((i, v), 2) | i <- [1..3], v <- alphabet, v /= V.replicate n sharp]
                  ++ [((i, V.replicate n sharp), 3) | i <- [1..3]]

isZero = singleton [V.fromList [0]]
isOne = singleton [V.fromList [1]]

plus = intersect (wellformed sigma 0 3) $ Dfa delta 0 (S.singleton 0)
  where delta = M.fromList [
          ((0, V.fromList [0, 0, 0]), 0),
          ((0, V.fromList [1, 0, 1]), 0),
          ((0, V.fromList [0, 1, 1]), 0),
          ((0, V.fromList [1, 1, 0]), 1),
          ((1, V.fromList [0, 0, 1]), 0),
          ((1, V.fromList [1, 0, 0]), 1),
          ((1, V.fromList [0, 1, 0]), 1),
          ((1, V.fromList [1, 1, 1]), 1)]

add = (Rel plus $ V.fromList [1,2,3]) /\ (Rel isZero $ V.fromList [3])
allInv = exists 2 $ exists 3 $ (Rel plus $ V.fromList [1,2,3]) /\ (Rel isZero $ V.fromList [3])
someInv = exists 1 $ allInv

allZero = Rel isZero $ V.fromList [1]
someZero = exists 1 $ allZero

allZeroOrNot = not allZero \/ allZero
allZeroOrNot' = exists 2 (Rel isZero $ V.fromList [2]) /\ (not allZero \/ allZero)

twoZero = Rel isZero (V.fromList [1]) /\ Rel isZero (V.fromList [2])
twoZero' = exists 1 $ exists 2 twoZero

same = Rel plus $ V.fromList [1,2,3]
same' = forall 1 $ exists 2 $ exists 3 same

allEvenOrSuccEven = forall 1 $ exists 2 $ (Rel plus (V.fromList [2,2,1])) \/ (exists 3 $ exists 4 $ (Rel plus (V.fromList [2,2,3])) /\ (Rel plus (V.fromList [1,4,3]) /\ (Rel isOne (V.fromList [4]))))

main :: IO ()
main = print $ decide sigma 0 (wellformed sigma 0) $ allEvenOrSuccEven
