{-# LANGUAGE ExistentialQuantification #-}

module FormalLanguage.Regular (
  -- * Data type
  Regular (..),
 
  -- * Construction
  emptyLng, totalLng, emptyWord, singleton, fromList,

  -- * Decision
  member, empty, total, finite, equal,

  -- * Conversion
  toList, toNfa, toDfa,

  -- * Closure properties
  union, unions, intersect, intersects, complement, concat, kleene, (\\),
  reverse, applyHom, applyInvHom, applyStrictHom, applyInvStrictHom, quotient,
) where

import Prelude hiding (concat, reverse)
import Data.Maybe
import qualified Data.Set.Extra as S
import qualified Data.List as L
import qualified Data.Map as M
import qualified Control.Arrow as A
type Set = S.Set
type Map = M.Map

data Regular sym
  = forall st. (Ord st) => Dfa (Map (st, sym) st) st (Set st)
  | forall st. (Ord st) => Nfa (Map (st, sym) (Set st)) st (Set st)
  | forall st. (Ord st) => RLin (Map st (Set ([sym], Maybe st))) st
  | forall st. (Ord st) => RLinNormal (Map st (Set (sym, Maybe st))) Bool st -- the boolean tells if there is an eps-rule s -> eps

emptyLng = Dfa M.empty () S.empty

totalLng sigma = complement sigma emptyLng

emptyWord = Dfa M.empty () (S.singleton ())

singleton :: (Ord sym) => [sym] -> Regular sym
singleton [] = emptyWord
singleton (sym:word) = extend $ singleton word
  where extend (Dfa delta s fin) = Dfa delta' (Left ()) fin'
          where delta' = M.insert (Left (), sym) (Right s) $
                  M.mapKeysMonotonic (A.first Right) $ M.map Right delta
                fin' = S.mapMonotonic Right fin

fromList :: (Ord sym) => [[sym]] -> Regular sym
fromList [] = emptyLng
fromList words = unions $ map singleton words

reachableDfa :: (Ord st) => Map (st, sym) st -> Set st -> Set st
reachableDfa delta sts = reachable sts sts
  where delta' = M.mapKeysWith S.union fst $ M.map S.singleton delta
        reachable checked toCheck | S.null toCheck' = toCheck
                                  | otherwise       = S.union toCheck $ reachable checked' toCheck'
          where toCheck' = S.concatMap (\q -> M.findWithDefault S.empty q delta') toCheck S.\\ checked
                checked' = S.union checked toCheck'

reachableDfa' :: (Ord st) => Map (st, sym) st -> Set st -> Set st
reachableDfa' delta = reachable S.empty
  where delta' = M.mapKeysWith S.union fst $ M.map S.singleton delta
        reachable checked toCheck | S.null toCheck' = S.empty
                                  | otherwise       = S.union toCheck' $ reachable checked' toCheck'
          where toCheck' = S.concatMap (\q -> M.findWithDefault S.empty q delta') toCheck S.\\ checked
                checked' = S.union checked toCheck'

statesDfa delta s = S.insert s $ S.map fst (M.keysSet delta) `S.union` S.fromList (M.elems delta)

statesNfa delta s = S.insert s $ S.map fst (M.keysSet delta) `S.union` S.unions (M.elems delta)

alphabet (Dfa delta _ _) = S.map snd $ M.keysSet delta
alphabet (Nfa delta _ _) = S.map snd $ M.keysSet delta

runDfa :: (Ord st, Ord sym) => Map (st, sym) st -> st -> [sym] -> Maybe st
runDfa delta q = delta' (Just q)
  where delta' Nothing _ = Nothing
        delta' x [] = x
        delta' (Just q) (sym:word) = delta' (M.lookup (q, sym) delta) word

runNfa delta q = delta' (S.singleton q)
  where delta' qs [] = qs
        delta' qs (sym:word) = delta' (S.concatMap (\q -> M.findWithDefault S.empty (q, sym) delta) qs) word

acceptsRLin rules q = run (S.singleton q)
  where epsClosure checked toCheck
          | S.null toCheck' = checked
          | otherwise       = epsClosure (checked `S.union` toCheck') toCheck'
          where toCheck' = S.concatMap (\q -> S.mapMaybe filterEpsTrans $ M.lookup q rules) toCheck
                filterEpsTrans (Just (Nothing, Just q)) = Just q
                filterEpsTrans _ = Nothing
        accepts qs [] = S.filter epsClosure qs
        accepts qs sym
        accepts qs (sym:word) = run (S.concatMap (\q -> S.mapMaybe filterSym M.lookup q rules)) word
          where filterSym (Just sym', Just q) | sym' == sym = 
                                              | otherwise   = Nothing


member :: (Ord sym) => [sym] -> Regular sym -> Bool
member word (Dfa delta s fin) = maybe False (`S.member` fin) $ runDfa delta s word
member word (Nfa delta s fin) = S.any (`S.member` fin) $ runNfa delta s word
member word (RLin rules s) =  rules s word

empty :: (Ord sym) => Regular sym -> Bool
empty (Dfa delta s fin) = S.all (`S.notMember` fin) $ reachableDfa delta (S.singleton s)
empty reg = empty $ toDfa reg

total :: (Ord sym) => Set sym -> Regular sym -> Bool
total sigma reg = empty $ complement sigma reg

-- iff there is a loop from a reachable state to itself and a final state can be reached from this state,
-- then the language is infinite
finite :: (Ord sym) => Regular sym -> Bool
finite dfa@(Dfa delta s fin) = not $ S.any (\q ->
  q `S.member` (reachableDfa' delta (S.singleton q))
  && S.any (`S.member` fin) (reachableDfa delta (S.singleton q)))
  (reachableDfa delta (S.singleton s))
finite reg = finite $ toDfa reg

equal :: (Ord sym) => Regular sym -> Regular sym -> Bool
equal reg1 reg2 = empty $ (reg1 \\ reg2) `union` (reg2 \\ reg1)

toList :: (Ord sym) => Regular sym -> [[sym]]
toList reg | empty reg = []
           | otherwise = [ word | word <- words, member word reg ]
           where words = [] : [ s : w | w <- words, s <- sigma ]
                 sigma = S.toList $ alphabet reg

toNfa :: Regular sym -> Regular sym
toNfa nfa@Nfa{} = nfa
toNfa (Dfa delta s fin) = Nfa delta' s fin
  where delta' = M.map S.singleton delta
toNfa (RLin rules s) = Nfa delta s fin
  where delta = 

toDfa :: (Ord sym) => Regular sym -> Regular sym
toDfa dfa@Dfa{} = dfa
toDfa nfa@(Nfa delta s fin) = Dfa delta' (S.singleton s) fin'
  where sigma = alphabet nfa
        lookup (qs, a) _ | S.null lookedUp = Nothing
                         | otherwise       = Just lookedUp
                         where lookedUp = S.concatMap (\q -> M.findWithDefault S.empty (q, a) delta) qs
        reachable map checked toCheck
          | S.null toCheck' = (M.union map map', checked)
          | otherwise       = reachable (M.union map map') (S.union checked toCheck') toCheck'
          where map' = M.mapMaybeWithKey lookup $ M.fromSet (const ()) $ S.cartesianProduct toCheck sigma
                toCheck' = S.fromList (M.elems map') S.\\ checked
        (delta', states') = reachable M.empty (S.singleton $ S.singleton s) (S.singleton $ S.singleton s)
        fin' = S.filter (S.any (`S.member` fin)) states'

toRLin (Nfa delta s fin) = RLin rules s
  where 

-- pairs (as, a) such that as:a is a real prefix of syms
prefixes syms = --TODO

toRLinNormal (RLin rules s) = RLinNormal rules' epsRule s
  where epsClosure checked toCheck
          | S.null toCheck' = checked
          | otherwise       = epsClosure (checked `S.union` toCheck') toCheck'
          where toCheck' = S.concatMap (\q -> S.mapMaybe filterEpsTrans $ M.lookup q rules) toCheck
                filterEpsTrans (Just (Nothing, Just q)) = Just q
                filterEpsTrans _ = Nothing
        epsRule = S.any (\q -> ([], Nothing) `S.elem` (fromJust $ M.lookup q rules)) $ epsClosure (S.singleton s) (S.singleton s)
        rules' = M.fromListWith S.union $ L.map (\(q, xs) -> S.concatMap (separate q) xs
          where separate q ([], Just q') = S.null
                --TODO remove state if epsilon can be reached from it
                separate q (syms, Just q') = S.map (\q'' -> ((q, L.init syms), (L.last syms, Just (q'', [])))) (epsClosure (S.singleton q') (S.singleton q'))
                  `S.union` S.fromList [((q, as), (a, Just (q, as ++ [a]))) | (as, a) <- prefixes syms]
                separate q ([], Nothing) = S.null
                separate q (syms, Nothing) = --TODO

-- construct the product automaton
productDfa :: (Ord st1, Ord st2, Ord sym)
  => Map (st1, sym) st1 -> Map (st2, sym) st2 -> Map ((st1, st2), sym) (st1, st2)
productDfa delta1 delta2 = delta
  where delta = M.mapKeys map $ M.fromSet lookup $ S.filter filter $
          S.cartesianProduct (M.keysSet delta1) (M.keysSet delta2)
        filter ((q1, a1), (q2, a2)) | a1 == a2  = True
                                    | otherwise = False
        lookup (k1, k2) = (fromJust $ M.lookup k1 delta1, fromJust $ M.lookup k2 delta2)
        map ((q1, a), (q2, _)) = ((q1, q2), a)

-- Left () is a new rejecting state
totalDfa :: (Ord st, Ord sym) =>
  Set sym -> Map (st, sym) st -> st -> Map (Either () st, sym) (Either () st)
totalDfa sigma delta s = M.union (M.mapKeysMonotonic (A.first Right) $ M.map Right delta)
  (M.fromSet (const $ Left ()) (S.cartesianProduct
  (S.insert (Left ()) $ S.mapMonotonic Right $ S.insert s $ statesDfa delta s) sigma))

union :: (Ord sym) => Regular sym -> Regular sym -> Regular sym
union dfa1@(Dfa delta1 s1 fin1) dfa2@(Dfa delta2 s2 fin2) = Dfa delta (Right s1, Right s2) fin
  where sigma = S.union (alphabet dfa1) (alphabet dfa2)
        delta = productDfa (totalDfa (alphabet dfa2) delta1 s1) (totalDfa (alphabet dfa1) delta2 s2)
        fin = S.union
          (S.cartesianProduct (S.mapMonotonic Right fin1)
            (S.insert (Left ()) $ S.mapMonotonic Right $ statesDfa delta2 s2))
          (S.cartesianProduct (S.insert (Left ()) $ S.mapMonotonic Right $ statesDfa delta1 s1)
            (S.mapMonotonic Right fin2))
union reg1 reg2 = (toDfa reg1) `union` (toDfa reg2)

-- | The union of multiple languages.
unions :: (Ord sym) => [Regular sym] -> Regular sym
unions (reg : regs) = foldl union reg regs

-- | The intersection of two languages.
intersect :: (Ord sym) => Regular sym -> Regular sym -> Regular sym
intersect (Dfa delta1 s1 fin1) (Dfa delta2 s2 fin2) = Dfa delta (s1, s2) fin
  where delta = productDfa delta1 delta2
        fin = S.cartesianProduct fin1 fin2
intersect reg1 reg2 = (toDfa reg1) `intersect` (toDfa reg2)

-- | The intersection of multiple languages.
intersects :: (Ord sym) => [Regular sym] -> Regular sym
intersects (reg : regs) = foldl intersect reg regs

(\\) :: (Ord sym) => Regular sym -> Regular sym -> Regular sym
-- efficient automaton for difference: http://infolab.stanford.edu/~ullman/ialc/spr10/slides/rs2.pdf page 7
dfa1@(Dfa delta1 s1 fin1) \\ (Dfa delta2 s2 fin2) = Dfa delta (s1, Right s2) fin
  where delta = productDfa delta1 delta2'
        fin = S.cartesianProduct fin1 $ S.insert (Left ()) $ S.map Right $ statesDfa delta2 s2 S.\\ fin2
        delta2' = totalDfa (alphabet dfa1) delta2 s2
reg1 \\ reg2 = (toDfa reg1) \\ (toDfa reg2)

complement :: (Ord sym) => Set sym -> Regular sym -> Regular sym
complement sigma (Dfa delta s fin) = Dfa delta' (Right s) fin'
  where delta' = totalDfa sigma delta s
        fin' = S.insert (Left ()) $ S.map Right $ statesDfa delta s S.\\ fin
complement sigma reg = complement sigma $ toDfa reg

-- | The concatenation of two languages.
concat :: (Ord sym) => Regular sym -> Regular sym -> Regular sym
concat (Nfa delta1 s1 fin1) (Nfa delta2 s2 fin2) = Nfa delta (Left s1) fin
  where delta = M.union
          (M.mapKeysMonotonic (A.first Left) $ M.map (\qs -> if S.any (`S.member` fin1) qs
            then S.insert (Right s2) $ S.mapMonotonic Left qs
            else S.mapMonotonic Left qs) delta1)
          (M.mapKeysMonotonic (A.first Right) $ M.map (S.mapMonotonic Right) delta2)
        fin = S.map Right fin2
concat reg1 reg2 = concat (toNfa reg1) (toNfa reg2)

-- | The iteration/Kleene closure of a regular language.
kleene :: Regular sym -> Regular sym
kleene (Nfa delta s fin) = Nfa delta' s fin'
  where delta' = M.map (\qs -> if S.any (`S.member` fin) qs then S.insert s qs else qs) delta
        fin' = S.singleton s
kleene reg = kleene $ toNfa reg

-- | A regular language containing the reverse of all words over the given alphabet in the given language.
reverse :: (Ord sym) => Regular sym -> Regular sym
reverse (Dfa delta s fin) = Nfa delta' (Left ()) fin'
  where delta' = M.fromListWith S.union $ L.concatMap (\((q, a), q') ->
          ((Right q', a), S.singleton $ Right q)
          : (if q' `S.member` fin then [((Left (), a), S.singleton $ Right q)] else [])) $ M.toList delta
        fin' | s `S.member` fin = S.fromList [Left (), Right s]
             | otherwise        = S.singleton $ Right s
reverse reg = reverse $ toDfa reg

prefixes xs = [(take i xs, xs !! i) | i <- [0 .. length xs - 1]]

-- | `applyHom` takes the alphabet of the original language, a homomorphism
-- and returns the image of the given language under the homomorphism.
--applyHom :: (Ord sym') => (sym -> [sym']) -> Regular sym -> Regular sym'
applyHom hom dfa@(Dfa delta s fin) = Nfa delta' s' fin'
  where delta' = M.fromListWith S.union $ L.concatMap (\((q, a), q') ->
          --TODO: treat case where `hom a` is empty
          (((q, init (hom a)), last (hom a)), S.singleton (q', []))
          : [(((q, xs), x), S.singleton (q, xs ++ [x])) | (xs, x) <- prefixes (hom a)]
          $ M.toList delta
        s' = (s, [])
        fin' = S.map (\q -> (q, [])) fin

-- I call a homomorphism that maps syms to syms strict
applyStrictHom :: (Ord sym') => (sym -> sym') -> Regular sym -> Regular sym'
applyStrictHom hom (Dfa delta s fin) = Nfa delta' s fin
  where delta' = M.mapKeysWith S.union (A.second hom) $ M.map S.singleton delta
applyStrictHom hom (Nfa delta s fin) = Nfa delta' s fin
  where delta' = M.mapKeysWith S.union (A.second hom) delta

applyInvHom :: (Ord sym, Ord sym') => Set sym' -> (sym' -> [sym]) -> Regular sym -> Regular sym'
applyInvHom sigma' hom (Dfa delta s fin) = Dfa delta' s fin
  where delta' = M.mapMaybeWithKey (\(q, a) _ -> runDfa delta q (hom a)) $
          M.fromSet (const ()) $ S.cartesianProduct (statesDfa delta s) sigma'
applyInvHom sigma' hom reg = applyInvHom sigma' hom $ toDfa reg

applyInvStrictHom :: (Ord sym, Ord sym') => Set sym' -> (sym' -> sym) -> Regular sym -> Regular sym'
applyInvStrictHom sigma' hom (Dfa delta s fin) = Dfa delta' s fin
  where delta' = M.mapMaybeWithKey (\(q, a') _ -> M.lookup (q, hom a') delta) $
          M.fromSet (const ()) (S.cartesianProduct (statesDfa delta s) sigma')
applyInvStrictHom sigma' hom (Nfa delta s fin) = Nfa delta' s fin
  where delta' = M.mapMaybeWithKey (\(q, a') _ -> M.lookup (q, hom a') delta) $
          M.fromSet (const ()) (S.cartesianProduct (statesNfa delta s) sigma')

-- https://en.wikipedia.org/wiki/Right_quotient
-- https://www.seas.upenn.edu/~cit596/notes/dave/closure5.html
quotient :: (Ord sym) => Regular sym -> Regular sym -> Regular sym
quotient (Dfa delta1 s1 fin1) reg2@(Dfa _ _ _) = Dfa delta1 s1 fin
  where fin = S.filter (\q -> not $ empty $ reg2 `intersect` Dfa delta1 q fin1) $ statesDfa delta1 s1
quotient reg1 reg2 = (toDfa reg1) `quotient` (toDfa reg2) -- `intersect` needs a DFA, so this is more efficient
