{-# LANGUAGE ExistentialQuantification #-}

module FormalLanguage.Regular (
  -- * Data type
  Regular (..),
 
  -- * Construction
  emptyLng, totalLng, emptyWord, singleton, fromList,

  -- * Decision
  member, empty, total, equal,

  -- * Conversion
  toList, toNfa, toDfa,

  -- * Closure properties
  union, unions, intersect, intersects, complement, concat, kleene, (\\), reverse,
  applyHom, applyInvHom, applyStrictHom, applyInvStrictHom, quotient,
) where

import Prelude hiding (concat, reverse)
import Data.Maybe
import qualified Data.Set.Extra as S
import qualified Data.List as L
import qualified Data.Map as M
type Set = S.Set
type Map = M.Map

data Regular sym
  = forall st. (Ord st) => Dfa [sym] (Map (st, sym) st) st (Set st)
  | forall st. (Ord st) => Nfa [sym] (Map (st, sym) (Set st)) st (Set st)
  | Complement (Regular sym)
  | Union [Regular sym]
  | Intersect [Regular sym]
  | Concat [Regular sym]
  | Kleene Regular sym
  | Quotient (Regular sym) (Regular sym)
  | Regerse (Regular sym)

emptyLng :: [sym] -> Regular sym
emptyLng sigma = Fsm sigma Map.empty () Set.empty

totalLng :: Regular sym
totalLng sigma = complement $ emptyLng sigma

-- | The regular language containing only the empty word.
emptyWord :: Regular sym
emptyWord = Dfa (\_ _ -> Right ()) (Left ()) (== Left ())

-- | A regular language containing a single word, i.e. list of symbols.
singleton :: (Eq sym) => [sym] -> Regular sym
singleton [] = emptyWord
singleton (sym:word) = extend $ singleton word
  where extend (Dfa delta s fin) = Dfa delta' s' fin'
          where delta' (Right q) a = Right $ delta q a
                delta' (Left (Left ())) a | a == sym = Right s
                delta' _ _ = Left (Right ()) -- rejecting state
                s' = Left (Left ())
                fin' (Left _) = False
                fin' (Right q) = fin q

fromList words = unions $ map singleton words

member word (Fsm _ delta s fin) = delta' (Just s) word
  where delta' Nothing _ = False
        delta' q [] = q `elem` fin
        delta' q (sym:word) = delta' $ M.lookup (q, sym) delta
member word (Complement reg) = not $ member word reg
member word (Union regs) = any (member word) regs
member word (Intersect regs) = all (member word) regs
--member word (Concat regs) = 
--Quotient
member word (Reverse reg) = member (reverse word) reg

reachableDfa delta = reachableNfa (map S.singleton delta)
reachableNfa delta st = reachable (S.singleton st) (S.singleton st)
  where delta' = mapKeysWith S.union fst
        reachable checked toCheck | S.null toCheck' = S.empty
                                  | otherwise       = S.union checked' $ reachable checked' toCheck'
          where toCheck' = S.concatMap $ (\q -> M.findWithDefault S.empty q delta') toCheck S.\\ checked
                checked' = S.union checked toCheck'

empty (Dfa _ delta s fin) = all (`S.notMember` fin) $ reachableDfa delta s
empty (Nfa _ delta s fin) = all (`S.notMember` fin) $ reachableDfa delta s
empty (Complement reg) = total reg
empty (Union regs) = all empty regs
empty (Intersect regs) = total regs
empty (Concat regs) = any empty regs
empty (Kleene _) = False
--empty (Quotient reg1 reg2) = 
empty (Reverse reg) = empty reg

getAlphabet (Dfa sigma _ _ _) = sigma
getAlphabet (Nfa sigma _ _ _) = sigma
getAlphabet (Complement reg) = getAlphabet reg
getAlphabet (Union (reg : regs)) = getAlphabet reg
getAlphabet (Intersect (reg : regs)) = getAlphabet reg
getAlphabet (Concat (reg : regs)) = getAlphabet reg
getAlphabet (Kleene reg) = getAlphabet reg
getAlphabet (Quotient reg1 reg2) = getAlphabet reg1

total (Dfa sigma delta s fin) = all (`S.member` fin) reachable
  && all (isJust . (`M.lookup` delta)) [ (q, a) | q <- S.toList reachable, a <- sigma ]
    where reachable = reachableDfa delta s
total (Complement reg) = empty reg
total (Union regs) = empty intersect $ map complement regs
total (Intersect regs) = all total regs
--total (Concat
total (Kleene reg) = all (\a -> [a] `member` reg) $ getAlphabet reg
--total (Quotient

--TODO finite: https://wwwmath.uni-muenster.de:16030/Professoren/Lippe/lehre/skripte/TheoretischeInformatik/TheoretischeInformatik.pdf Seite 28

equal reg1 reg2 = empty sigma $ (reg1 \\ reg2) `union` (reg2 \\ reg1)

-- | Return a list of all words over the alphabet that are member of the given language.
-- This will not terminate unless the language is empty, i.e. even if the language is finite.
toList :: Regular sym -> [[sym]]
toList reg | empty reg = []
           | otherwise = [word | word <- words, member word reg]
           where words = [] : [ s : w | w <- words, s <- getAlphabet reg ]

toNfa :: Regular sym -> Regular sym
toNfa nfa@Nfa{} = nfa
toNfa (Dfa delta s fin) = Nfa delta' s fin
  where delta' q (Just sym) = S.singleton $ delta q sym
        delta' _ Nothing = S.empty

toDfa :: Regular sym -> Regular sym
toDfa dfa@Dfa{} = dfa
toDfa (Nfa delta s fin) = Dfa delta' s' fin'
  where delta' states sym = S.concatMap (`delta` Just sym) (epsTrans states)
        s' = S.singleton s
        fin' states = S.any fin (epsTrans states)
        -- returns `states` plus states reachable via epsilon transitions
        epsTrans states | newStates `S.isSubsetOf` states = states
                        | otherwise                       = epsTrans $ S.union states newStates
                        where newStates = S.concatMap (`delta` Nothing) states

union reg1 reg2 = Union [reg1, reg2]

unions = Union

intersect reg1 reg2 = Intersect [reg1, reg2]

intersects = Intersect

complement = Complement

--concat reg1 reg2 = concat (toNfa reg1) (toNfa reg2)

-- | The iteration/Kleene closure of a regular language.
kleene (Nfa delta s fin) = Nfa delta' s fin'
  where delta' q Nothing
          | fin q     = S.insert s $ delta q Nothing
          | otherwise = delta q Nothing
        delta' q sym = delta q sym
        -- there is a transition from any final state to the start state, so this works
        -- also, it guarantees that the empty word is accepted
        fin' q = q == s
kleene reg = kleene $ toNfa reg

reg1 \\ reg2 = intersect reg1 $ complement reg2

reverse = Reverse

applyHom sigma hom (Nfa delta s fin) = Nfa delta' s' fin'
  where -- return empty set if `word ++ [sym]` is not prefix of any word in the image of the homomorphism
        delta' (q, word) (Just sym)
          | any (L.isPrefixOf newWord) image = S.insert (q, newWord) $
              mapOrig $ S.unions $ map (delta q . Just) $ filter (\x -> hom x == newWord) sigma
          | otherwise = S.empty
          where newWord = word ++ [sym]
        delta' (q, []) Nothing = mapOrig $ S.union (delta q Nothing) $
          S.unions $ map (delta q . Just) $ filter (null . hom) sigma
        delta' _ _ = S.empty
        s' = (s, [])
        fin' (q, []) = fin q
        fin' _ = False
        mapOrig = S.map (\y -> (y, []))
        image = map hom sigma
applyHom sigma hom reg = applyHom sigma hom $ toNfa reg

-- a map sym -> hom^-1(sym)
reverseMap [] hom = M.empty
reverseMap (sym:sigma) hom = M.insertWith (++) (hom sym) [sym] $ reverseMap sigma hom

-- I call a homomorphism that maps syms to syms strict
applyStrictHom :: (Ord sym') => [sym] -> (sym -> sym') -> Regular sym -> Regular sym'
applyStrictHom sigma hom (Dfa delta s fin) = Nfa delta' s fin
  where delta' q (Just sym') | isNothing lookedUp = S.empty
                             | otherwise          = fromJust lookedUp
                             where lookedUp = M.lookup (q, sym') deltaMap
        delta' q Nothing = S.empty
        reachable = S.toList $ reachableDfa sigma delta s
        deltaMap = M.fromList [((q, sym'), S.fromList $ map (delta q) syms) | q <- reachable, (sym', syms) <- M.assocs $ reverseMap sigma hom]
applyStrictHom sigma hom (Nfa delta s fin) = Nfa delta' s fin
  where delta' q (Just sym') | isNothing lookedUp = S.empty
                            | otherwise          = fromJust lookedUp
                            where lookedUp = M.lookup (q, sym') deltaMap
        delta' q Nothing = delta q Nothing
        reachable = S.toList $ reachableNfa sigma delta s
        deltaMap = M.fromList [((q, sym'), S.unions $ map (delta q . Just) syms) | q <- reachable, (sym', syms) <- M.assocs $ reverseMap sigma hom]

applyInvHom :: (sym' -> [sym]) -> Regular sym -> Regular sym'
applyInvHom hom (Dfa delta s fin) = Dfa delta' s fin
  where delta' q sym = foldl delta q $ hom sym
applyInvHom hom nfa@Nfa{} = applyInvHom hom nfa

applyInvStrictHom :: (sym' -> sym) -> Regular sym -> Regular sym'
applyInvStrictHom hom (Dfa delta s fin) = Dfa delta' s fin
  where delta' q sym = delta q $ hom sym
applyInvStrictHom hom (Nfa delta s fin) = Nfa delta' s fin
  where delta' q Nothing = delta q Nothing
        delta' q (Just sym) = delta q $ Just $ hom sym

-- https://en.wikipedia.org/wiki/Right_quotient
quotient :: [sym] -> Regular sym -> Regular sym -> Regular sym
-- https://www.seas.upenn.edu/~cit596/notes/dave/closure5.html
quotient sigma (Dfa delta1 s1 fin1) reg2 = Dfa delta1 s1 (`S.member` finSet)
  where fin q = not $ empty sigma $ intersect reg2 $ Dfa delta1 q fin1
        finSet = S.filter fin $ reachableDfa sigma delta1 s1
quotient sigma (Nfa delta1 s1 fin1) reg2 = Nfa delta1 s1 (`S.member` finSet)
  where fin q = not $ empty sigma $ intersect reg2 $ Nfa delta1 q fin1
        finSet = S.filter fin $ reachableNfa sigma delta1 s1
