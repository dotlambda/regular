{-# LANGUAGE ExistentialQuantification #-}

module FormalLanguage.Regular (
  -- * Data type
  Regular (..),
 
  -- * Construction
  emptyLng, totalLng, emptyWord, singleton, fromList,

  -- * Decision
  member, empty, total, equal,

  -- * Conversion
  toList, toNfa, toDfa,

  -- * Closure properties
  union, unions, intersect, intersects, complement, concat, kleene, (\\), reverse,
  applyHom, applyInvHom, applyStrictHom, applyInvStrictHom, quotient,
) where

import Prelude hiding (concat, reverse)
import Data.Maybe
import qualified Data.Set.Extra as S
import qualified Data.List as L
import qualified Data.Map as M
type Set = S.Set

-- | The data type for regular languages
data Regular sym
  -- Instead of writing a DFA with a partial function delta,
  -- use an NFA and make the function return the empty set
  -- | The constructor for deterministic finite automata.
  -- It requires a transition function delta that takes a state, a symbol and returns the next state,
  -- a starting state s and a function fin returning for every state if is final.
  = forall st. (Ord st) => Dfa (st -> sym -> st) st (st -> Bool)
  -- | The constructor for non-deterministic finite automata.
  -- In contrast to the one for DFAs, the transition function returns a set of possible follow-up states.
  | forall st. (Ord st) =>  Nfa (st -> Maybe sym -> Set st) st (st -> Bool)
  --TODO NFA without epsilon tansitiions

emptyLng :: Regular sym
emptyLng = Dfa (\_ _ -> ()) () (const False)

totalLng :: Regular sym
totalLng = complement emptyLng

-- | The regular language containing only the empty word.
emptyWord :: Regular sym
emptyWord = Dfa (\_ _ -> Right ()) (Left ()) (== Left ())

-- | A regular language containing a single word, i.e. list of symbols.
singleton :: (Eq sym) => [sym] -> Regular sym
singleton [] = emptyWord
singleton (sym:word) = extend $ singleton word
  where extend (Dfa delta s fin) = Dfa delta' s' fin'
          where delta' (Right q) a = Right $ delta q a
                delta' (Left (Left ())) a | a == sym = Right s
                delta' _ _ = Left (Right ()) -- rejecting state
                s' = Left (Left ())
                fin' (Left _) = False
                fin' (Right q) = fin q

-- | Build an automaton from a finite list of words.
fromList :: (Eq sym) => [[sym]] -> Regular sym
fromList words = unions $ map singleton words

reachableDfa sigma delta s = reachable (S.singleton s) (S.singleton s)
  where reachable checked toCheck
          | S.null nextStates = checked
          | otherwise           = reachable (S.union checked nextStates) nextStates
          where nextStates = S.concatMap (\q -> S.fromList $ map (delta q) sigma) toCheck S.\\ checked

reachableNfa sigma delta s = reachable (S.singleton s) (S.singleton s)
  where reachable checked toCheck
          | S.null nextStates = checked
          | otherwise         = reachable (S.union checked nextStates) nextStates
          where nextStates = S.concatMap (\q -> S.union
                          (S.unions $ map (delta q . Just) sigma)
                          (delta q Nothing)) toCheck S.\\ checked

-- | Decide if a given word is in a given regular language.
member :: [sym] -> Regular sym -> Bool
member word (Dfa delta s fin) = fin $ foldl delta s word
member word reg = member word (toDfa reg)

-- | Decide if the given language is contains no word over the given alphabet.
empty :: [sym] -> Regular sym -> Bool
empty sigma (Dfa delta s fin) = not $ S.any fin $ reachableDfa sigma delta s
empty sigma (Nfa delta s fin) = not $ S.any fin $ reachableNfa sigma delta s

-- | Decide if the given language contains every word over the given alphabet.
total :: [sym] -> Regular sym -> Bool
total sigma reg = empty sigma $ complement reg

--TODO finite: https://wwwmath.uni-muenster.de:16030/Professoren/Lippe/lehre/skripte/TheoretischeInformatik/TheoretischeInformatik.pdf Seite 28

-- | Decide if two languages are equal over the given alphabet
equal :: [sym] -> Regular sym -> Regular sym -> Bool
equal sigma reg1 reg2 = empty sigma $ (reg1 \\ reg2) `union` (reg2 \\ reg1)

-- | Return a list of all words over the alphabet that are member of the given language.
-- This will not terminate unless the language is empty, i.e. even if the language is finite.
toList :: [sym] -> Regular sym -> [[sym]]
toList sigma reg | empty sigma reg = []
                 | otherwise       = [word | word <- words, member word reg]
                 where words = [] : [ s : w | w <- words, s <- sigma ]

toNfa :: Regular sym -> Regular sym
toNfa nfa@Nfa{} = nfa
toNfa (Dfa delta s fin) = Nfa delta' s fin
  where delta' q (Just sym) = S.singleton $ delta q sym
        delta' _ Nothing = S.empty

toDfa :: Regular sym -> Regular sym
toDfa dfa@Dfa{} = dfa
toDfa (Nfa delta s fin) = Dfa delta' s' fin'
  where delta' states sym = S.concatMap (`delta` Just sym) (epsTrans states)
        s' = S.singleton s
        fin' states = S.any fin (epsTrans states)
        -- returns `states` plus states reachable via epsilon transitions
        epsTrans states | newStates `S.isSubsetOf` states = states
                        | otherwise                       = epsTrans $ S.union states newStates
                        where newStates = S.concatMap (`delta` Nothing) states

-- new states are pairs of old states and the automaton accepts if `op` of the entries is a final state
combineWith op (Dfa delta1 s1 fin1) (Dfa delta2 s2 fin2) = Dfa delta s fin
  where delta (q1,q2) sym = (delta1 q1 sym, delta2 q2 sym)
        s = (s1,s2)
        fin (q1,q2) = fin1 q1 `op` fin2 q2
combineWith op (Nfa delta1 s1 fin1) (Nfa delta2 s2 fin2) = Nfa delta s fin
  where delta (q1,q2) Nothing = S.cartesianProduct
          (S.insert q1 $ delta1 q1 Nothing)
          (S.insert q2 $ delta2 q2 Nothing)
        delta (q1,q2) sym = S.cartesianProduct (delta1 q1 sym) (delta2 q2 sym)
        s = (s1,s2)
        fin (q1,q2) = fin1 q1 `op` fin2 q2
combineWith op reg1 reg2 = combineWith op (toNfa reg1) (toNfa reg2)

-- | The union of two languages.
union :: Regular sym -> Regular sym -> Regular sym
union = combineWith (||)

-- | The union of multiple languages.
unions :: Foldable t => t (Regular sym) -> Regular sym
unions = foldl union emptyLng

-- | The intersection of two languages.
intersect :: Regular sym -> Regular sym -> Regular sym
intersect = combineWith (&&)

-- | The intersection of multiple languages.
intersects :: Foldable t => t (Regular sym) -> Regular sym
intersects = foldl intersect totalLng

complement :: Regular sym -> Regular sym
complement (Dfa delta s fin) = Dfa delta s (not . fin)
complement reg = complement $ toDfa reg

-- | The concatenation of two languages.
concat :: Regular sym -> Regular sym -> Regular sym
concat (Nfa delta1 s1 fin1) (Nfa delta2 s2 fin2) = Nfa delta s fin
  where delta (Left q1) Nothing
          | fin1 q1 = S.insert (Right s2) (S.map Left $ delta1 q1 Nothing)
        delta (Left q1) sym = S.map Left $ delta1 q1 sym
        delta (Right q2) sym = S.map Right $ delta2 q2 sym
        s = Left s1
        fin (Left _) = False
        fin (Right q2) = fin2 q2
concat reg1 reg2 = concat (toNfa reg1) (toNfa reg2)

-- | The iteration/Kleene closure of a regular language.
kleene (Nfa delta s fin) = Nfa delta' s fin'
  where delta' q Nothing
          | fin q     = S.insert s $ delta q Nothing
          | otherwise = delta q Nothing
        delta' q sym = delta q sym
        -- there is a transition from any final state to the start state, so this works
        -- also, it guarantees that the empty word is accepted
        fin' q = q == s
kleene reg = kleene $ toNfa reg

-- | The difference of two regular languages.
(\\) :: Regular sym -> Regular sym -> Regular sym
-- efficient automaton for difference: http://infolab.stanford.edu/~ullman/ialc/spr10/slides/rs2.pdf page 7
reg1 \\ reg2 = intersect reg1 $ complement reg2

-- | A regular language containing the reverse of all words over the given alphabet in the given language.
reverse :: [sym] -> Regular sym -> Regular sym
reverse sigma (Nfa delta s fin) = Nfa delta' s' fin'
  where delta' (Left ()) Nothing = S.map Right $ S.filter fin allStates
        delta' (Left ()) _ = S.empty
        delta' (Right q) sym = S.map Right $ S.filter (\q' -> q `S.member` delta q' sym) allStates
        s' = Left ()
        fin' (Left _) = False
        fin' (Right q) = q == s
        allStates = reachableNfa sigma delta s
reverse sigma reg = reverse sigma $ toNfa reg

-- | `applyHom` takes the alphabet of the original language, a homomorphism
-- and returns the image of the given language under the homomorphism.
applyHom :: (Ord sym') => [sym] -> (sym -> [sym']) -> Regular sym -> Regular sym'
-- similar to `map`, therefore this order of arguments
applyHom sigma hom (Nfa delta s fin) = Nfa delta' s' fin'
  where -- return empty set if `word ++ [sym]` is not prefix of any word in the image of the homomorphism
        delta' (q, word) (Just sym)
          | any (L.isPrefixOf newWord) image = S.insert (q, newWord) $
              mapOrig $ S.unions $ map (delta q . Just) $ filter (\x -> hom x == newWord) sigma
          | otherwise = S.empty
          where newWord = word ++ [sym]
        delta' (q, []) Nothing = mapOrig $ S.union (delta q Nothing) $
          S.unions $ map (delta q . Just) $ filter (null . hom) sigma
        delta' _ _ = S.empty
        s' = (s, [])
        fin' (q, []) = fin q
        fin' _ = False
        mapOrig = S.map (\y -> (y, []))
        image = map hom sigma
applyHom sigma hom reg = applyHom sigma hom $ toNfa reg

-- a map sym -> hom^-1(sym)
reverseMap [] hom = M.empty
reverseMap (sym:sigma) hom = M.insertWith (++) (hom sym) [sym] $ reverseMap sigma hom

-- I call a homomorphism that maps syms to syms strict
applyStrictHom :: (Ord sym') => [sym] -> (sym -> sym') -> Regular sym -> Regular sym'
applyStrictHom sigma hom (Dfa delta s fin) = Nfa delta' s fin
  where delta' q (Just sym') | isNothing lookedUp = S.empty
                             | otherwise          = fromJust lookedUp
                             where lookedUp = M.lookup (q, sym') deltaMap
        delta' q Nothing = S.empty
        reachable = S.toList $ reachableDfa sigma delta s
        deltaMap = M.fromList [((q, sym'), S.fromList $ map (delta q) syms) | q <- reachable, (sym', syms) <- M.assocs $ reverseMap sigma hom]
applyStrictHom sigma hom (Nfa delta s fin) = Nfa delta' s fin
  where delta' q (Just sym') | isNothing lookedUp = S.empty
                            | otherwise          = fromJust lookedUp
                            where lookedUp = M.lookup (q, sym') deltaMap
        delta' q Nothing = delta q Nothing
        reachable = S.toList $ reachableNfa sigma delta s
        deltaMap = M.fromList [((q, sym'), S.unions $ map (delta q . Just) syms) | q <- reachable, (sym', syms) <- M.assocs $ reverseMap sigma hom]

applyInvHom :: (sym' -> [sym]) -> Regular sym -> Regular sym'
applyInvHom hom (Dfa delta s fin) = Dfa delta' s fin
  where delta' q sym = foldl delta q $ hom sym
applyInvHom hom nfa@Nfa{} = applyInvHom hom nfa

applyInvStrictHom :: (sym' -> sym) -> Regular sym -> Regular sym'
applyInvStrictHom hom (Dfa delta s fin) = Dfa delta' s fin
  where delta' q sym = delta q $ hom sym
applyInvStrictHom hom (Nfa delta s fin) = Nfa delta' s fin
  where delta' q Nothing = delta q Nothing
        delta' q (Just sym) = delta q $ Just $ hom sym

-- https://en.wikipedia.org/wiki/Right_quotient
quotient :: [sym] -> Regular sym -> Regular sym -> Regular sym
-- https://www.seas.upenn.edu/~cit596/notes/dave/closure5.html
quotient sigma (Dfa delta1 s1 fin1) reg2 = Dfa delta1 s1 (`S.member` finSet)
  where fin q = not $ empty sigma $ intersect reg2 $ Dfa delta1 q fin1
        finSet = S.filter fin $ reachableDfa sigma delta1 s1
quotient sigma (Nfa delta1 s1 fin1) reg2 = Nfa delta1 s1 (`S.member` finSet)
  where fin q = not $ empty sigma $ intersect reg2 $ Nfa delta1 q fin1
        finSet = S.filter fin $ reachableNfa sigma delta1 s1
