# regular-language

A Haskell library for defining regular languages, i.e. FSA, and making use of their closure properties.

If you have no idea what that is, look [here](https://en.wikipedia.org/wiki/Regular_language) and [here](https://en.wikipedia.org/wiki/Deterministic_finite_automaton).

## Installation

`cabal install regular-language`

## Documentation

https://hackage.haskell.org/package/regular-language
