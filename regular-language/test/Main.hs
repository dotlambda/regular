import Test.HUnit
import Test.Framework
import Test.Framework.Providers.HUnit
import Test.Framework.Runners.Console

import Prelude hiding (concat, reverse)
import Data.Maybe
import qualified Data.Set as S
import qualified Data.Map as M
import FormalLanguage.Regular

nfa = Nfa (M.fromList
  $ ((0, 1), S.fromList [1 .. 2])
  : [((i, 0), S.singleton 0) | i <- [0 .. 2]]
  ++ [((i, 1), S.singleton 0) | i <- [1 .. 2]])
  0 (S.singleton 1)
dfa = Dfa (M.fromList [((0, 1), 1), ((1, 1), 0), ((0, 0), 0), ((1, 0), 0)]) 0 (S.singleton 1)

main :: IO()
main = defaultMain $ map (\x -> testCase "" $ assert x) [
  not $ member [1,0,1] $ kleene $ complement (S.fromList [0, 1]) $ concat dfa $ complement (S.fromList [0, 1]) $ intersect dfa nfa,
  total (S.fromList [0,1]) $ kleene $ complement (S.fromList [0, 1]) emptyWord,
  not $ empty (emptyWord :: Regular Int),
  not $ member [0,0,1] (reverse nfa),
  member [1,0,0,1] (reverse nfa),
  total (S.fromList [0,1]) (union nfa $ complement (S.fromList [0, 1]) dfa),
  empty (intersect dfa $ complement (S.fromList [0, 1]) nfa),
  not $ member "a" (fromList list),
  equal nfa dfa,
  equal (fromList ["", "aba", "abaaba", "abaabaaba"]) $ applyHom (\x -> if x=='a' then "aba" else "") $ fromList list,
  --equal (fromList list) $ applyInvStrictHom sigma hom $ reverse $ reverse $ toDfa $ applyStrictHom hom $ fromList list,
  (==) (S.fromList list) $ S.fromList $ take 5 $ toList $ union (fromList list) $ fromList list,
  --equal (fromList list) $ reverse $ reverse $ fromList list,
  (==) [[],["b"],["a","a"],["a","b"],["a","a","a"]] $ take 5 $ toList $ applyStrictHom (\x -> if x=='a' then "a" else "b") $ fromList list,
  --member "a" $ quotient (fromList list) (fromList list),
  member [0,1,1,0,1,0,0,0,1,0] $ quotient nfa dfa
  ]
  where --nfa = Nfa (\x y -> if isNothing y then S.empty else if y>=Just 1 && x==0 then S.fromList [x+1..x+251] else S.singleton 0) 0 (\x -> x==1) :: Regular Int
        --dfa = Dfa (\x y -> if y>0 && x==0 then 1 else 0) 0 (==1) :: Regular Int
        list = ["aa","aaa","","ab","b"]
        sigma = S.fromList ['a','b']
        sigma' = S.fromList ["aba","b"]
        hom x = if x=='a' then "aba" else "b"
