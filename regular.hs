import Data.List (union, intersect)

-- https://stackoverflow.com/questions/15327160/haskell-merging-multiple-lists
merge :: (Eq a) => [[a]] -> [a]
merge [] = []
merge (x:xs) = union x $ merge xs

unionMap :: (Eq b) => (a -> [b]) -> [a] -> [b] 
unionMap f xs = merge $ map f xs

data State = State String deriving (Eq, Show)

data Transition = Transition State String State

-- return a list of possible follow-up states given state s and input c
next :: [Transition] -> String -> State -> [State]
next [] _ _ = []
next ((Transition r b t):xs) c s
  | r == s && b == c = t:(next xs c s)
  | otherwise        = next xs c s

-- return a list of possible follow-up states given any of states and the empty word as input
nextEmpty :: [Transition] -> [State] -> [State]
nextEmpty xs states 
  | nextStates == states = states
  | otherwise            = nextEmpty xs nextStates
  where nextStates = merge (states:(map (next xs "") states))

reachable :: [Transition] -> [State] -> [String] -> [State]
reachable xs states [] = nextEmpty xs states
reachable xs states ("":input) = reachable xs states input
reachable xs states (c:input)
  | nextStatesEmpty == states = reachable xs (unionMap (next xs c) states) input
  | otherwise                 = reachable xs (nextStatesEmpty ++ states) (c:input)
  where nextStatesEmpty = nextEmpty xs states 

data Regular = Nfa State [Transition] [State] | RLin | LLin

accepts :: Regular -> [String] -> Bool
accepts (Nfa initial transitions finals) input = not $ null $ intersect reachableStates finals
  where reachableStates = reachable transitions [initial] input

-- Example: Nfa accepting an even number of 0's:
-- accepts (Nfa (State "a") [Transition (State "b") "0" (State "a"),Transition (State "a") "0" (State "b")] [(State "a")]) ["0","0"]
