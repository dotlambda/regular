module Regular (
  -- * Regular type
  Regular,
  
  -- * Construction
  emptyLng, emptyWord, singleton, fromList,
  
  -- * Decision
  member, empty, total, equal,

  -- * Conversion
  toList, toNfa, toDfa,

  -- * Closure properties
  union, unions, intersect, complement, Regular.concat, kleene, difference, symmetricDiff, Regular.reverse,
  applyHom, applyInvHom, applyStrictHom, applyInvStrictHom,
) where

import qualified Data.Set.Extra as Set
import qualified Data.List as List
type Set = Set.Set

data Regular st sym =
  -- Instead of writing a DFA with a partial function delta,
  -- use an NFA and make the function return the empty set
  Dfa (Set sym) (st -> sym -> st) st (st -> Bool) |
  Nfa (Set sym) (st -> Maybe sym -> Set st) st (st -> Bool)
  --deriving (Show)

emptyLng sigma = Dfa sigma (\_ _ -> 1) 1 (== 0)

emptyWord sigma = Dfa sigma (\_ _ -> -1) 0 (== 0)

singleton :: (Eq st, Num st, Eq sym) => Set sym -> [sym] -> Regular st sym
singleton sigma [] = emptyWord sigma
singleton sigma (symbol:word) = extend $ singleton sigma word
  where extend (Dfa _ delta s fin) = Dfa sigma delta' s' fin
          where delta' q a
                  | q /= s'     = delta q a
                  | a == symbol = s
                  | otherwise   = -1 -- `-1` is a rejecting state
                s' = s + 1

fromList sigma words = unions $ map (singleton sigma) words

reachableStates :: (Ord st, Ord sym) => Regular st sym -> Set st
reachableStates (Dfa sigma delta s _) = reachable (Set.singleton s) (Set.singleton s)
  where reachable checked toCheck
          | Set.null nextStates = checked
          | otherwise           = reachable (Set.union checked nextStates) nextStates
          where nextStates = Set.concatMap (\state -> Set.map (delta state) sigma) toCheck Set.\\ checked
reachableStates (Nfa sigma delta s _) = reachable (Set.singleton s) (Set.singleton s)
  where reachable checked toCheck
          | Set.null nextStates = checked
          | otherwise           = reachable (Set.union checked nextStates) nextStates
          where nextStates = Set.concatMap (\state -> Set.union
                  (Set.concatMap (\symbol -> delta state (Just symbol)) sigma)
                  (delta state Nothing)) toCheck Set.\\ checked

--TODO error if symbol is not in sigma
member :: (Ord st) => [sym] ->  Regular st sym ->  Bool
member word (Dfa _ delta s fin) = fin $ deltaStar s word
  where deltaStar q [] = q
        deltaStar q (symbol:word) = deltaStar (delta q symbol) word
member word reg = member word (toDfa reg)

empty (Dfa sigma delta s fin) = not $ Set.any fin (reachableStates (Dfa sigma delta s fin))
empty (Nfa sigma delta s fin) = not $ Set.any fin (reachableStates (Nfa sigma delta s fin))

total reg = empty $ complement reg

equal regular1 regular2 = empty $ symmetricDiff regular1 regular2

toList :: (Ord st) => Regular st sym -> [[sym]]
toList (Dfa sigma delta s fin) = [word | word <- words, member word (Dfa sigma delta s fin)]
  where words = [] : [ s:w | w <- words, s <- symbols]
        symbols = Set.toList sigma
toList regular = toList $ toDfa regular

toNfa (Nfa sigma delta s fin) = Nfa sigma delta s fin
toNfa (Dfa sigma delta s fin) = Nfa sigma delta' s fin
  where delta' q (Just symbol) = Set.singleton $ delta q symbol
        delta' _ Nothing = Set.empty

toDfa :: (Ord st) => Regular st sym -> Regular (Set st) sym
-- a new Dfa where state `s` is represented by `Set.singleton s`
toDfa (Dfa sigma delta s fin) = Dfa sigma delta' s' fin'
  where -- `states` has only one element, so we just apply delta to that one
        delta' states symbol = Set.singleton $ delta (Set.elemAt 0 states) symbol
        s' = Set.singleton s
        fin' = Set.any fin
toDfa (Nfa sigma delta s fin) = Dfa sigma delta' s' fin'
  where delta' states symbol = Set.concatMap (\state -> delta state (Just symbol)) (epsTrans states)
        s' = Set.singleton s
        fin' states = Set.any fin (epsTrans states)
        -- returns `states` plus states reachable via epsilon transitions
        epsTrans states
          | newStates `Set.isSubsetOf` states = states
          | otherwise                         = epsTrans $ Set.union states newStates
          -- be careful not to create an infinite loop when defining a delta function like `(\x y -> Set.singleton x+1)`
          where newStates = Set.concatMap (\state -> delta state Nothing) states

union :: (Ord st1, Ord st2, Ord sym) => Regular st1 sym -> Regular st2 sym -> Regular (st1,st2) sym
-- union for DFAs: new states are pairs of old states and the automaton accepts if one of the entries is a final state
union (Dfa sigma1 delta1 s1 fin1) (Dfa sigma2 delta2 s2 fin2) = Dfa sigma delta s fin
  where sigma = Set.union sigma1 sigma2
        delta (q1,q2) symbol = (delta1 q1 symbol, delta2 q2 symbol)
        s = (s1,s2)
        fin (q1,q2) = fin1 q1 || fin2 q2
-- other possibility for NFAs: add a new start state and two epsilon transitions to the old start states
union (Nfa sigma1 delta1 s1 fin1) (Nfa sigma2 delta2 s2 fin2) = Nfa sigma delta s fin
  where sigma = Set.union sigma1 sigma2
        delta (q1,q2) Nothing = Set.cartesianProduct
          (Set.insert q1 $ delta1 q1 Nothing)
          (Set.insert q2 $ delta2 q2 Nothing)
        delta (q1,q2) symbol = Set.cartesianProduct (delta1 q1 symbol) (delta2 q2 symbol)
        s = (s1,s2)
        fin (q1,q2) = fin1 q1 || fin2 q2
union regular1 regular2 = union (toNfa regular1) (toNfa regular2)

unions :: (Ord st, Ord sym) => [Regular st sym] -> Regular (Either () (Int,st)) sym
unions [] = Nfa sigma delta s fin
  where sigma = Set.empty
        delta (Left ()) _ = Set.empty
        s = Left ()
        fin = const False
unions ((Nfa sigma1 delta1 s1 fin1):regulars) = extend $ unions regulars
  where extend (Nfa sigma2 delta2 s2 fin2) = Nfa sigma delta s2 fin
          where sigma = Set.union sigma1 sigma2
                delta (Left ()) Nothing = Set.insert (Right (count,s1)) $ delta2 (Left ()) Nothing
                delta (Right (x,q)) a
                  | x == count = Set.map (\state -> Right (count,state)) $ delta1 q a
                delta q a = delta2 q a
                fin (Right (x,q))
                  | x == count = fin1 q
                fin q = fin2 q
                count = Set.size $ delta2 (Left ()) Nothing
unions (regular:regulars) = unions $ (toNfa regular):regulars

intersect :: (Ord st1, Ord st2, Ord sym) => Regular st1 sym -> Regular st2 sym -> Regular (st1,st2) sym
intersect (Dfa sigma1 delta1 s1 fin1) (Dfa sigma2 delta2 s2 fin2) = Dfa sigma delta s fin
  where sigma = Set.union sigma1 sigma2
        delta (q1,q2) symbol = (delta1 q1 symbol, delta2 q2 symbol)
        s = (s1,s2)
        fin (q1,q2) = fin1 q1 && fin2 q2
intersect (Nfa sigma1 delta1 s1 fin1) (Nfa sigma2 delta2 s2 fin2) = Nfa sigma delta s fin
  where sigma = Set.union sigma1 sigma2
        delta (q1,q2) Nothing = Set.cartesianProduct
          (Set.insert q1 $ delta1 q1 Nothing)
          (Set.insert q2 $ delta2 q2 Nothing)
        delta (q1,q2) symbol = Set.cartesianProduct (delta1 q1 symbol) (delta2 q2 symbol)
        s = (s1,s2)
        fin (q1,q2) = fin1 q1 && fin2 q2
intersect regular1 regular2 = intersect (toNfa regular1) (toNfa regular2)

complement regular = negate $ toDfa regular
  where negate (Dfa sigma delta s fin) = Dfa sigma delta s (not . fin)

concat :: (Ord st1, Ord st2, Ord sym) => Regular st1 sym -> Regular st2 sym -> Regular (Either st1 st2) sym
concat (Nfa sigma1 delta1 s1 fin1) (Nfa sigma2 delta2 s2 fin2) = Nfa sigma delta s fin
  where sigma = Set.union sigma1 sigma2
        delta (Left q1) Nothing
          | fin1 q1 = Set.insert (Right s2) (Set.map Left $ delta1 q1 Nothing)
        delta (Left q1) symbol = Set.map Left $ delta1 q1 symbol
        delta (Right q2) symbol = Set.map Right $ delta2 q2 symbol
        s = Left s1
        fin (Left _) = False
        fin (Right q2) = fin2 q2
concat regular1 regular2 = Regular.concat (toNfa regular1) (toNfa regular2)

kleene :: (Ord st) => Regular st sym -> Regular st sym
kleene (Nfa sigma delta s fin) = Nfa sigma delta' s fin'
  where delta' q Nothing
          | fin q     = Set.insert s $ delta q Nothing
          | otherwise = delta q Nothing
        delta' q symbol = delta q symbol
        -- there is a transition from any final state to the start state, so this works
        -- also, it guarantees that the empty word is accepted
        fin' q = (q == s)
kleene regular = kleene $ toNfa regular

-- regular1 \\ reglular2
difference regular1 regular2 = intersect regular1 $ complement regular2

-- https://en.wikipedia.org/wiki/Symmetric_difference
symmetricDiff regular1 regular2 = union (difference regular1 regular2) (difference regular2 regular1)

reverse :: (Ord st, Ord sym) => Regular st sym -> Regular (Either () st) sym
reverse (Nfa sigma delta s fin) = Nfa sigma delta' s' fin'
  where delta' (Left ()) Nothing = Set.map Right $ Set.filter fin allStates
        delta' (Left ()) _ = Set.empty
        delta' (Right q) symbol = Set.map Right $ Set.filter (\q' -> q `Set.member` delta q' symbol) allStates
        s' = Left ()
        fin' (Left _) = False
        fin' (Right q) = q == s
        allStates = reachableStates (Nfa sigma delta s fin)
reverse regular = Regular.reverse $ toNfa regular

-- similar to `map`, therefore this order of arguments
applyHom :: (Ord st, Ord sym, Ord sym') => (sym -> [sym']) -> Regular st sym -> Regular (st, [sym']) sym'
applyHom hom (Nfa sigma delta s fin) = Nfa sigma' delta' s' fin'
  where sigma' = Set.concatMap Set.fromList image
        -- return empty set if `symbol:word` is not prefix of any word in the image of the homomorphism
        delta' (q, word) (Just symbol)
          | Set.any (List.isPrefixOf newWord) image = Set.insert (q, newWord) $
              mapOrig $ Set.concatMap (\x -> delta q $ Just x) $ Set.filter (\x -> hom x == newWord) sigma
          | otherwise = Set.empty
          where newWord = word ++ [symbol]
        delta' (q, []) Nothing = mapOrig $ Set.union (delta q Nothing) $
          Set.concatMap (\x -> delta q $ Just x) $ Set.filter (\x -> hom x == []) sigma
        delta' _ _ = Set.empty
        s' = (s, [])
        fin' (q, []) = fin q
        fin' _ = False
        mapOrig = Set.map (\y -> (y, []))
        image = Set.map hom sigma
applyHom hom regular = applyHom hom $ toNfa regular

-- I call a homomorphism that maps symbols to symbols strict
applyStrictHom :: (Ord sym') => (sym -> sym') -> Regular st sym -> Regular st sym'
applyStrictHom hom (Dfa sigma delta s fin) = Dfa image delta' s fin
  where delta' q symbol = delta q $ Set.elemAt 0 $ Set.filter (\x -> hom x == symbol) sigma
        image = Set.map hom sigma
applyStrictHom hom (Nfa sigma delta s fin) = Nfa image delta' s fin
  where delta' q (Just symbol) = delta q $ Just $ Set.elemAt 0 $ Set.filter (\x -> hom x == symbol) sigma
        delta' q Nothing = delta q Nothing
        image = Set.map hom sigma

applyInvHom :: (Ord st, Eq sym) => (sym' -> [sym]) -> Set sym' -> Regular st sym -> Regular (Set st) sym'
-- `sigma'` cannot be computed because you'd have to enumerate all instances of `sym'`
applyInvHom hom sigma' regular = applyTo $ toDfa regular
  where applyTo (Dfa sigma delta s fin) = Dfa preimage delta' s fin
          where preimage = Set.filter (List.all (`elem` sigma) . hom) sigma'
                delta' q symbol = deltaStar q $ hom symbol
                deltaStar q [] = q
                deltaStar q (symbol:word) = deltaStar (delta q symbol) word

applyInvStrictHom hom sigma' (Dfa sigma delta s fin) = Dfa preimage delta' s fin
  where preimage = Set.filter ((`elem` sigma) . hom) sigma'
        delta' q symbol = delta q $ hom symbol
applyInvStrictHom hom sigma' (Nfa sigma delta s fin) = Nfa preimage delta' s fin
  where preimage = Set.filter ((`elem` sigma) . hom) sigma'
        delta' q Nothing = delta q Nothing
        delta' q (Just symbol) = delta q $ Just $ hom symbol

-- https://en.wikipedia.org/wiki/Right_quotient
-- https://www.seas.upenn.edu/~cit596/notes/dave/closure5.html
quotient (Dfa sigma1 delta1 s1 fin1) reg2 = Dfa sigma1 delta1 s1 fin
  where fin q = not $ empty $ intersect reg2 $ (Dfa sigma1 delta1 q fin1)
quotient (Nfa sigma1 delta1 s1 fin1) reg2 = Nfa sigma1 delta1 s1 fin
  where fin q = not $ empty $ intersect reg2 $ (Nfa sigma1 delta1 q fin1)

main :: IO()
main = do
  print $ (==) False $ isFin (toDfa nfa) (Set.singleton 2)
  print $ (==) False $ member [1,0,1] $ kleene $ complement $ Regular.concat dfa $ complement $ intersect dfa nfa
  print $ (==) True $ total (kleene $ complement $ emptyWord (Set.fromList [0,1])) --[0,1,0,1]
  print $ (==) False $ empty $ emptyWord $ Set.fromList [0,1]
  -- this line is quite fast
  print $ (==) False $ member [0,0,1] (Regular.reverse nfa)
  -- this line takes forever
  print $ (==) True $ member [1,0,0,1] (Regular.reverse nfa)
  print $ (==) True $ total (union nfa $ complement dfa)
  print $ (==) True $ empty (intersect dfa $ complement nfa)
  print $ (==) False $ member "a" (fromList sigma list)
  print $ (==) True $ equal nfa dfa
  print $ (==) True $ equal (fromList sigma ["", "aba", "abaaba", "abaabaaba"]) $ applyHom (\x -> if x=='a' then "aba" else "") $ fromList sigma list
  print $ (==) True $ equal (fromList sigma list) $ applyInvStrictHom hom sigma $ Regular.reverse $ Regular.reverse $ toDfa $ applyStrictHom hom $ fromList sigma list
  print $ (==) True $ equal (fromList sigma list) $ Regular.reverse $ Regular.reverse $ fromList sigma list
  print $ (==) [[],["b"],["a","a"],["a","b"],["a","a","a"]] $ take 5 $ toList $ applyStrictHom (\x -> if x=='a' then "a" else "b") $ fromList sigma list
  print $ (==) True $ member "a" $ quotient (fromList sigma list) (fromList sigma list)
  where nfa = Nfa (Set.fromList [0,1]) (\x y -> if y==Nothing then Set.empty else if y>=Just 1 && x==0 then Set.fromList [x+1..x+251] else Set.singleton 0) 0 (\x -> x==1) :: Regular Int Int
        dfa = Dfa (Set.fromList [0,1]) (\x y -> if y>0 && x==0 then 1 else 0) 0 (==1) :: Regular Int Int
        isFin (Dfa _ _ _ fin) state = fin state
        list = ["aa","aaa","","b","ab"]
        sigma = Set.fromList ['a','b']
        hom = \x -> if x=='a' then "aba" else "b"
