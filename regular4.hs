module Regular (
  -- * Regular type
  Regular,
  
  -- * Construction
  emptyLng, emptyWord, singleton, fromList,
  
  -- * Decision
  member, empty, total, equal,

  -- * Conversion
  toList, toNfa, toDfa,

  -- * Closure properties
  Regular.union, unions, Regular.intersect, complement, Regular.concat, kleene, difference, symmetricDiff, Regular.reverse,
) where

import Data.List as L

unionMap f xs = foldl L.union [] $ map f xs

cartesianProduct xs ys = [ (x,y) | x <- xs, y <- ys]

data Regular st sym =
  -- Instead of writing a DFA with a partial function delta,
  -- use an NFA and make the function return the empty set
  Dfa [sym] (st -> sym -> st) st (st -> Bool) |
  Nfa [sym] (st -> Maybe sym -> [st]) st (st -> Bool)
  --deriving (Show)

emptyLng sigma = Dfa sigma (\_ _ -> 1) 1 (== 0)

emptyWord sigma = Dfa sigma (\_ _ -> -1) 0 (== 0)

--singleton :: (Num st) => [sym] -> [sym] -> Regular st sym
singleton sigma [] = emptyWord sigma
singleton sigma (symbol:word) = extend $ singleton sigma word
  where extend (Dfa _ delta s fin) = Dfa sigma delta' s' fin
          where delta' q a
                  | q /= s'     = delta q a
                  | a == symbol = s
                  | otherwise   = -1 -- `-1` is a rejecting state
                s' = s + 1

fromList sigma words = unions $ map (singleton sigma) words

reachableStates :: (Eq st) => Regular st sym -> [st]
reachableStates (Dfa sigma delta s _) = reachable [s] [s]
  where reachable checked toCheck
          | null nextStates = checked
          | otherwise       = reachable (L.union checked nextStates) nextStates
          where nextStates = unionMap (\state -> map (delta state) sigma) toCheck \\ checked
reachableStates (Nfa sigma delta s _) = reachable [s] [s]
  where reachable checked toCheck
          | null nextStates = checked
          | otherwise       = reachable (L.union checked nextStates) nextStates
          where nextStates = unionMap (\state -> L.union
                  (unionMap (\symbol -> delta state (Just symbol)) sigma)
                  (delta state Nothing)) toCheck \\ checked

--TODO error if symbol is not in sigma
member :: (Eq st) => Regular st sym -> [sym] -> Bool
member (Dfa _ delta s fin) word = fin $ delta' s word
  where delta' state [] = state
        delta' state (symbol:word) = delta' (delta state symbol) word
member regular word = member (toDfa regular) word

--TODO rename empty to null?
--empty :: (Eq st) => Regular st sym -> Bool
empty (Dfa sigma delta s fin) = not $ any fin (reachableStates (Dfa sigma delta s fin))
empty (Nfa sigma delta s fin) = not $ any fin (reachableStates (Nfa sigma delta s fin))

total :: (Eq st) => Regular st sym -> Bool
total = empty . complement

equal regular1 regular2 = empty $ symmetricDiff regular1 regular2

toList :: (Eq st) => Regular st sym -> [[sym]]
toList (Dfa sigma delta s fin) = [word | word <- words, member (Dfa sigma delta s fin) word]
  where words = [] : [ s:w | w <- words, s <- sigma]
toList regular = toList $ toDfa regular

toNfa :: Regular st sym -> Regular st sym
toNfa (Nfa sigma delta s fin) = Nfa sigma delta s fin
toNfa (Dfa sigma delta s fin) = Nfa sigma delta' s fin
  where delta' state (Just symbol) = [delta state symbol]
        delta' _ Nothing = []

toDfa :: (Eq st) => Regular st sym -> Regular [st] sym
-- a new Dfa where state `s` is represented by `Set.singleton s`
toDfa (Dfa sigma delta s fin) = Dfa sigma delta' s' fin'
  where -- `states` has only one element, so we just apply delta to that one
        delta' states symbol = [delta (head states) symbol]
        s' = [s]
        fin' = any fin
toDfa (Nfa sigma delta s fin) = Dfa sigma delta' s' fin'
  where delta' states symbol = unionMap (\state -> delta state (Just symbol)) (epsTrans states)
        s' = [s]
        fin' states = any fin (epsTrans states)
        -- returns `states` plus states reachable via epsilon transitions
        epsTrans states
          | all (`elem` states) newStates = states
          | otherwise                   = epsTrans $ L.union states newStates
          -- be careful not to create an infinite loop when defining a delta function like `(\x y -> Set.singleton x+1)`
          where newStates = unionMap (\state -> delta state Nothing) states

union :: (Eq sym) => Regular st1 sym -> Regular st2 sym -> Regular (st1,st2) sym
-- union for DFAs: new states are pairs of old states and the automaton accepts if one of the entries is a final state
union (Dfa sigma1 delta1 s1 fin1) (Dfa sigma2 delta2 s2 fin2) = Dfa sigma delta s fin
  where sigma = L.union sigma1 sigma2
        delta (q1,q2) symbol = (delta1 q1 symbol, delta2 q2 symbol)
        s = (s1,s2)
        fin (q1,q2) = fin1 q1 || fin2 q2
-- other possibility for NFAs: add a new start state and two epsilon transitions to the old start states
union (Nfa sigma1 delta1 s1 fin1) (Nfa sigma2 delta2 s2 fin2) = Nfa sigma delta s fin
  where sigma = L.union sigma1 sigma2
        delta (q1,q2) Nothing = cartesianProduct
          (q1 : (delta1 q1 Nothing))
          (q2 : (delta2 q2 Nothing))
        delta (q1,q2) symbol = cartesianProduct (delta1 q1 symbol) (delta2 q2 symbol)
        s = (s1,s2)
        fin (q1,q2) = fin1 q1 || fin2 q2
union regular1 regular2 = Regular.union (toNfa regular1) (toNfa regular2)

unions :: (Eq sym) => [Regular st sym] -> Regular (Either () (Int,st)) sym
unions [] = Nfa sigma delta s fin
  where sigma = []
        delta (Left ()) _ = []
        s = Left ()
        fin = const False
unions ((Nfa sigma1 delta1 s1 fin1):regulars) = extend $ unions regulars
  where extend (Nfa sigma2 delta2 s2 fin2) = Nfa sigma delta s2 fin
          where sigma = L.union sigma1 sigma2
                delta (Left ()) Nothing = (Right (count,s1)) : (delta2 (Left ()) Nothing)
                delta (Right (x,q)) a
                  | x == count = map (\state -> Right (count,state)) $ delta1 q a
                delta q a = delta2 q a
                fin (Right (x,q))
                  | x == count = fin1 q
                fin q = fin2 q
                count = length $ delta2 (Left ()) Nothing
unions (regular:regulars) = unions $ (toNfa regular) : regulars

intersect :: (Eq sym) => Regular st1 sym -> Regular st2 sym -> Regular (st1,st2) sym
intersect (Dfa sigma1 delta1 s1 fin1) (Dfa sigma2 delta2 s2 fin2) = Dfa sigma delta s fin
  where sigma = L.union sigma1 sigma2
        delta (q1,q2) symbol = (delta1 q1 symbol, delta2 q2 symbol)
        s = (s1,s2)
        fin (q1,q2) = fin1 q1 && fin2 q2
intersect (Nfa sigma1 delta1 s1 fin1) (Nfa sigma2 delta2 s2 fin2) = Nfa sigma delta s fin
  where sigma = L.union sigma1 sigma2
        delta (q1,q2) Nothing = cartesianProduct
          (q1 : (delta1 q1 Nothing))
          (q2 : (delta2 q2 Nothing))
        delta (q1,q2) symbol = cartesianProduct (delta1 q1 symbol) (delta2 q2 symbol)
        s = (s1,s2)
        fin (q1,q2) = fin1 q1 && fin2 q2
intersect regular1 regular2 = Regular.intersect (toNfa regular1) (toNfa regular2)

complement :: (Eq st) => Regular st sym -> Regular [st] sym
complement regular = negate $ toDfa regular
  where negate (Dfa sigma delta s fin) = Dfa sigma delta s (not . fin)

concat :: (Eq sym) => Regular st1 sym -> Regular st2 sym -> Regular (Either st1 st2) sym
concat (Nfa sigma1 delta1 s1 fin1) (Nfa sigma2 delta2 s2 fin2) = Nfa sigma delta s fin
  where sigma = L.union sigma1 sigma2
        delta (Left q1) Nothing
          | fin1 q1 = (Right s2) : (map Left $ delta1 q1 Nothing)
        delta (Left q1) symbol = map Left $ delta1 q1 symbol
        delta (Right q2) symbol = map Right $ delta2 q2 symbol
        s = Left s1
        fin (Left _) = False
        fin (Right q2) = fin2 q2
concat regular1 regular2 = Regular.concat (toNfa regular1) (toNfa regular2)

kleene :: (Eq st) => Regular st sym -> Regular st sym
kleene (Nfa sigma delta s fin) = Nfa sigma delta' s fin'
  where delta' q Nothing
          | fin q     = s : (delta q Nothing)
          | otherwise = delta q Nothing
        delta' q symbol = delta q symbol
        -- there is a transition from any final state to the start state, so this works
        -- also, it guarantees that the empty word is accepted
        fin' q = (q == s)
kleene regular = kleene $ toNfa regular

-- regular1 \\ reglular2
difference regular1 regular2 = Regular.intersect regular1 $ complement regular2

-- https://en.wikipedia.org/wiki/Symmetric_difference
symmetricDiff regular1 regular2 = Regular.union (difference regular1 regular2) (difference regular2 regular1)

reverse :: (Eq st) => Regular st sym -> Regular (Either () st) sym
reverse (Nfa sigma delta s fin) = Nfa sigma delta' s' fin'
  where delta' (Left ()) Nothing = [Right q' | q' <- allStates, fin q']
        delta' (Left ()) _ = [] 
        delta' (Right q) symbol = [Right q' | q' <- allStates, elem q $ delta q' symbol]
        s' = Left ()
        fin' (Left _) = False
        fin' (Right q) = fin q
        allStates = reachableStates (Nfa sigma delta s fin)
reverse regular = Regular.reverse $ toNfa regular

--TODO (inverse) homomorphism, quotient (https://en.wikipedia.org/wiki/Right_quotient)

main :: IO()
main = do
  --print $ isFin (toDfa nfa) [2]
  --print $ member (kleene $ complement $ Regular.concat dfa $ complement $ Regular.intersect dfa nfa) [1,0,1]
  --print $ total (kleene $ complement $ emptyWord [0,1]) --[0,1,0,1]
  --print $ empty $ emptyWord [0,1]
  -- this line is quite fast
  --print $ member (Regular.reverse nfa) [0,0,1]
  -- this line takes forever
  --print $ member (Regular.reverse nfa) [1,0,0,1]
  --print $ total (Regular.union nfa $ complement dfa)
  --print $ empty (Regular.intersect dfa $ complement nfa)
  --print $ member (fromList sigma list) "a"
  --print $ equal nfa dfa
  --print $ toList (fromList sigma list)
  -- sadly, this won't work:
  print $ empty (fromList sigma $ toList $ emptyWord sigma)
  where nfa = Nfa [0,1] (\x y -> if y==Nothing then [] else if y>=Just 1 && x==0 then [x+1..x+1000000] else [0]) 0 (\x -> x==1) :: Regular Int Int
        dfa = Dfa [0,1] (\x y -> if y>0 && x==0 then 1 else 0) 0 (==1) :: Regular Int Int
        isFin (Dfa _ _ _ fin) state = fin state
        list = ["aa","aaa","aaaa",""]
        sigma = ['a','b']
