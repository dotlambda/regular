import qualified Data.Set as Set
type Set = Set.Set

-- http://stackoverflow.com/questions/6428279/why-data-set-has-no-powerset-function
powerset s
  | s == Set.empty = Set.singleton Set.empty
  | otherwise      = Set.map (Set.insert x) pxs `Set.union` pxs
      where (x, xs) = Set.deleteFindMin s
            pxs = powerset xs

unionMap :: (Ord b) => (a -> Set b) -> Set a -> Set b
-- apply f to each element of set s and union the resulting sets
unionMap f s = Set.fold Set.union Set.empty (Set.map f s)

data Regular st sym =
  -- Instead of writing a DFA with a partial function delta,
  -- use an NFA and make the function return the empty set
  Dfa (Set sym) (Set st) (st -> sym -> st) st (Set st) |
  Nfa (Set sym) (Set st) (st -> Maybe sym -> Set st) st (Set st)
  --deriving (Show)

accepts :: (Ord st) => Regular st sym -> [sym] -> Bool
accepts (Dfa sigma sts delta s fin) word = Set.member (delta' s word) fin
  where delta' q [] = q
        delta' q (symbol:word) = delta' (delta q symbol) word
accepts regular word = accepts (toDfa regular) word

toNfa :: Regular st sym -> Regular st sym
toNfa (Nfa sigma sts delta s fin) = Nfa sigma sts delta s fin
toNfa (Dfa sigma sts delta s fin) = Nfa sigma sts delta' s fin
  where delta' state (Just symbol) = Set.singleton $ delta state symbol
        delta' _ Nothing = Set.empty

toDfa :: (Ord st) => Regular st sym -> Regular (Set st) sym
-- a new Dfa where state `s` is represented by `Set.singleton s`
toDfa (Dfa sigma sts delta s fin) = Dfa sigma sts' delta' s' fin'
  where sts' = Set.map (\x -> Set.singleton x) sts
        -- `states` has only 1 element, so we just apply delta to that one
        delta' states symbol = Set.singleton $ delta (head $ Set.toList states) symbol
        s' = Set.singleton s
        fin' = Set.map (\x -> Set.singleton x) fin
toDfa (Nfa sigma sts delta s fin) = Dfa sigma sts' delta' s' fin'
  where sts' = powerset sts
        delta' states symbol = unionMap (\state -> delta state (Just symbol)) (epsTrans states)
        s' = Set.singleton s
        fin' = Set.filter (\states -> not $ Set.null $ Set.intersection fin (epsTrans states)) sts'
        -- returns `states` plus states reachable via epsilon transitions
        epsTrans states
          | newStates `Set.isSubsetOf` states = states
          | otherwise                         = epsTrans $ Set.union states newStates
          where newStates = unionMap (\state -> delta state Nothing) states

union :: (Ord st0, Ord st1, Ord sym) => Regular st0 sym -> Regular st1 sym
  -> Regular (Either (st0,st1) (Either () (Either st0 st1))) sym
-- union for DFAs: new states are pairs of old states and the automaton accepts if one of the entries is a final state
union (Dfa sigma0 sts0 delta0 s0 fin0) (Dfa sigma1 sts1 delta1 s1 fin1) =
  Dfa sigma sts delta s fin
    where sigma = Set.union sigma0 sigma1
          sts = Set.fromList [Left (q0,q1) | q0 <- Set.toList sts0, q1 <- Set.toList sts1]
          delta (Left (q0,q1)) symbol = Left (delta0 q0 symbol, delta1 q1 symbol)
          s = Left (s0,s1)
          fin = Set.filter (\(Left (q0,q1)) -> (Set.member q0 fin0) || (Set.member q1 fin1)) sts
-- union for NFAs: add a new start state and two epsilon transitions to the old start states
union (Nfa sigma0 sts0 delta0 s0 fin0) (Nfa sigma1 sts1 delta1 s1 fin1) =
  Nfa sigma sts delta s fin
    where sigma = Set.union sigma0 sigma1
          sts = Set.unions [Set.singleton $ Right (Left ()), mapLeft sts0, mapRight sts1]
          delta (Right (Left ())) Nothing = Set.fromList [Right (Right (Left s0)), Right (Right (Right s1))]
          delta (Right (Right (Left q0))) symbol =  mapLeft (delta0 q0 symbol)
          delta (Right (Right (Right q1))) symbol = mapRight (delta1 q1 symbol)
          s = Right (Left ())
          fin = Set.union (mapLeft fin0) (mapRight fin1)
          mapLeft = Set.map (Right . Right . Left)
          mapRight = Set.map (\x -> Right (Right (Right x)))
union m0 m1 = union (toNfa m0) (toNfa m1)

--TODO intersect, complement, concat, kleene, (inverse) homomorphism, quotient (https://en.wikipedia.org/wiki/Right_quotient), reverse

main :: IO()
main = do
  --print $ getFin $ toDfa nfa
  print $ accepts nfa []
  where nfa = Nfa (Set.fromList [0,1]) (Set.fromList [0..18]) (\x y -> Set.empty) 0 (Set.fromList [0]) :: Regular Int Int
        getFin (Dfa _ _ _ _ fin) = fin
